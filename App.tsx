import Store from './src/utils/redux/Store';
import {Provider} from 'react-redux';
import AppNavigation from './src/utils/navigation/AppNavigation';
import SplashScreen from 'react-native-splash-screen';
import {useEffect} from 'react';

export default function App() {
  useEffect(() => {
    SplashScreen.hide();
  }, []);
  return (
    <Provider store={Store}>
      <AppNavigation />
    </Provider>
  );
}
