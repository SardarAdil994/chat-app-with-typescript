import {configureStore} from '@reduxjs/toolkit';

import authenticationSlice from './Authentication';
import UsersSlice from './Users';

const store = configureStore({
  reducer: {
    authentication: authenticationSlice,
    users: UsersSlice,
  },
});
export type RootState = ReturnType<typeof store.getState>;
export default store;
