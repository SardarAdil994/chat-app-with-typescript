import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {User} from '../Types';
type initial = {
  allUsers: User[];
  currentUser: User;
};
const initialState: initial = {
  allUsers: [],
  currentUser: {
    name: '',
    email: '',
    contact: '',
    location: {
      latitude: '',
      longitude: '',
    },
    isActive: false,
    showLocation: false,
    id: '',
    uid: '',
    deviceToken: '',
  },
};
const UsersSlice = createSlice({
  name: 'authentication',
  initialState,
  reducers: {
    setAllUsers: (state, action) => {
      state.allUsers = action.payload.allUsers;
    },
    setCurrentUser: (state, action) => {
      state.currentUser = action.payload.currentUser;
    },
  },
});

export const setAllUsers = UsersSlice.actions.setAllUsers;
export const setCurrentUser = UsersSlice.actions.setCurrentUser;
export default UsersSlice.reducer;
