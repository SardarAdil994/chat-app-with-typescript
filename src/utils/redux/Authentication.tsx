import {createSlice} from '@reduxjs/toolkit';
import AsyncStorage from '@react-native-async-storage/async-storage';

export type Authentication = {
  token: string;
};

const authenticationSlice = createSlice({
  name: 'authentication',
  initialState: {
    token: '',
  },
  reducers: {
    login: (state, action) => {
      state.token = action.payload.token;
      AsyncStorage.setItem('token', action.payload.token);
    },
    logout: state => {
      state.token = '';
      AsyncStorage.removeItem('token').then(() => {});
    },
    signup: (state, action) => {},
    setLogin: (state, action) => {
      state.token = action.payload.token;
    },
  },
});

export const login = authenticationSlice.actions.login;
export const logout = authenticationSlice.actions.logout;
export const signup = authenticationSlice.actions.signup;
export const setLogin = authenticationSlice.actions.setLogin;
export default authenticationSlice.reducer;
