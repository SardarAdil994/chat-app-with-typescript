import messaging from '@react-native-firebase/messaging';
import PushNotification from 'react-native-push-notification';

PushNotification.createChannel({
  channelId: 'fcm_fallback_notification_channel',
  channelName: 'My channel',
  channelDescription: 'A channel to categorise your notifications',
  soundName: 'default',
  importance: 4,
  vibrate: true,
});

export const Listener = navigation => {
  messaging().onNotificationOpenedApp(remoteMessage => {
    console.log('Inside on notification opened app');
    if (remoteMessage) {
      const url = remoteMessage.data.url;
      const uid = remoteMessage.data.id;
      const deviceToken = remoteMessage.data.deviceToken;
      console.log(uid, deviceToken, remoteMessage.data);
      navigation.navigate(url, {
        data: {
          uid: uid,
          deviceToken: deviceToken,
        },
      });
    }
  });

  messaging().onMessage(async remoteMessage => {
    PushNotification.localNotification({
      channelId: 'fcm_fallback_notification_channel',
      message: remoteMessage.notification.body,
      title: remoteMessage.notification.title,
      smallIcon: 'https://cdn-icons-png.flaticon.com/128/2058/2058768.png',
      data: {
        url: remoteMessage.data.url,
        id: remoteMessage.data.id,
        deviceToken: remoteMessage.data.deviceToken,
      },
    });
  });

  messaging()
    .getInitialNotification()
    .then(remoteMessage => {
      console.log('Inside get inital notification');
      if (remoteMessage) {
        const url = remoteMessage.data.url;
        const userId = remoteMessage.data.id;
        const devc = remoteMessage.data.id;
        navigation.navigate(remoteMessage.data.url, {
          data: {
            uid: userId,
            deviceToken: devc,
          },
        });
      }
    });
};
