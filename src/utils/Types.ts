export interface User{
    name:string,
    email:string,
    contact:string,
    location:{
        latitude:string,
        longitude:string
    }
    isActive:boolean,
    showLocation:boolean,
    id:string,
    uid:string,
    deviceToken:string
}
export interface Users{
    users:User[]
}
export interface Authentication{
    token:string
}