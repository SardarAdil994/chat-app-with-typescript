import PushNotification from 'react-native-push-notification';

export const LocalNotificationListener = navigation => {
  PushNotification.configure({
    onNotification: function (notification) {
      if (
        notification.data.url &&
        notification.data.id &&
        notification.data.deviceToken
      ) {
        navigation.navigate(notification.data?.url, {
          data: {
            uid: notification.data?.id,
            deviceToken: notification.data?.deviceToken,
          },
        });
      }
    },
  });
};
