import React, {useLayoutEffect, useState, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {useSelector} from 'react-redux';
import {useDispatch} from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import firestore from '@react-native-firebase/firestore';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import Login from '../../screens/Login';
import Users from '../../screens/Users';
import Map from '../../screens/Map';
import {setLogin} from '../redux/Authentication';
import Signup from '../../screens/Signup';
import Chat from '../../screens/ChatScreen';
import Account from '../../screens/Account';
import {setCurrentUser} from '../redux/Users';

const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator();

function AppNavigation() {
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();
  const token = useSelector((state: any) => state.authentication.token);
  useLayoutEffect(() => {
    setIsLoading(true);
    AsyncStorage.getItem('token')
      .then((res: any) => {
        setUser(res);
        dispatch(setLogin({token: res}));
        setIsLoading(false);
      })
      .catch((err: any) => {
        console.log(err);
      });
  }, [token]);

  const setUser = async (token: string) => {
    try {
      const querySnap = await firestore()
        .collection('users')
        .where('uid', '==', token)
        .get();
      firestore()
        .collection('users')
        .doc(querySnap.docs[0].id)
        .update({
          ...querySnap.docs[0].data(),
          id: querySnap.docs[0].id,
          isActive: true,
        });
      dispatch(
        setCurrentUser({
          currentUser: {
            ...querySnap.docs[0].data(),
            id: querySnap.docs[0].id,
          },
        }),
      );
    } catch (error) {
      console.log('getData', error);
    }
  };
  const AuthenticationNav = () => {
    return (
      <Stack.Navigator>
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="signup"
          //@ts-ignore
          component={Signup}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    );
  };
  const HomeTabs = () => {
    return (
      <Tabs.Navigator
        screenOptions={{
          tabBarActiveTintColor: '#d76f39',
          tabBarInactiveTintColor: '#39405b',
          tabBarHideOnKeyboard: true,
          tabBarStyle: {
            position: 'absolute',
            backgroundColor: '#fcfcfc',
            elevation: 4,
            borderTopWidth: 0,
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
          },
        }}
        sceneContainerStyle={{backgroundColor: 'grey'}}>
        <Tabs.Screen
          name="users"
          component={Users}
          options={{
            headerShown: false,
            tabBarShowLabel: false,
            tabBarIcon: ({color, size}) => (
              <MaterialCommunityIcons name="home" color={color} size={26} />
            ),
          }}
        />
        <Tabs.Screen
          name="map"
          component={Map}
          options={{
            headerShown: false,
            tabBarShowLabel: false,
            tabBarIcon: ({color, size}) => (
              <MaterialCommunityIcons name="map" color={color} size={26} />
            ),
          }}
        />
        <Tabs.Screen
          name="account"
          component={Account}
          options={{
            headerShown: false,
            tabBarShowLabel: false,
            tabBarIcon: ({color, size}) => (
              <MaterialCommunityIcons
                name="account-circle-outline"
                color={color}
                size={26}
              />
            ),
          }}
        />
      </Tabs.Navigator>
    );
  };
  const AuthenticatedNav = () => {
    return (
      <Stack.Navigator
        screenOptions={{headerShown: false, animationEnabled: false}}>
        <Stack.Screen name="home" component={HomeTabs} />
        <Stack.Screen name="chat" component={Chat} />
      </Stack.Navigator>
    );
  };
  return (
    <NavigationContainer>
      {!token && !isLoading && <AuthenticationNav />}
      {token && !isLoading && <AuthenticatedNav />}
    </NavigationContainer>
  );
}

export default AppNavigation;
