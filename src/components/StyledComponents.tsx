import styled from 'styled-components';

export const StyledContainer = styled.ScrollView`
  flex: 1;
  background-color: ${(props: string) => props.color || 'white'};
  width: 100%;
  padding-top: 10px;
`;
export const StyledButton = styled.TouchableOpacity`
  width: 90%;
  justify-content: center;
  align-items: center;
  height: ${(props: string) => props.height || '30px'};
  background-color: #d76f39;
  border-radius: ${(props: string) => props.height || '30px'};
  margin-vertical: 10px;
`;
export const TextInputContainer = styled.View`
  width: 85%;
  height: 30px;
  margin-vertical: 20px;
  border-bottom-width: 1px;
  border-bottom-color: grey;
  padding: 0px;
  color: black;
  flex-direction: row;
  align-items: center;
  background-color: ${(props: string) => props.color || 'transparent'};
`;
export const TextInput = styled.TextInput`
  padding-horizontal: 20px;
  margin: 0px;
  padding-vertical: 0px;
  width: 85%;
`;
export const StyledText = styled.Text`
  font-size: ${(props: string) => props.fontSize || '30px'};
  font-weight: ${(props: string) => props.fontWeight || 'normal'};
  color: ${(props: string) => props.color || '30px'};
  text-decoration-line: ${(props: string) => props.line || 'none'};
  text-transform: ${(props: string) => props.transform || 'none'};
  text-align: ${(props: string) => props.align || 'left'};
`;
export const CustomContainer = styled.View`
  flex-direction: ${(props: string) => props.direction || 'column'};
  width: ${(props: string) => props.width || '100%'};
  height: ${(props: string) => props.height || '100%'};
  padding: ${(props: string) => props.pad || '0px'};
  margin-top: ${(props: string) => props.marginTop || '0px'};
  margin: ${(props: string) => props.margin || '0px'};
  background-color: ${(props: string) => props.color || 'transparent'};
  justify-content: ${(props: string) => props.justify || 'flex-start'};
  align-items: ${(props: string) => props.align || 'flex-start'};
  flex-wrap: ${(props: string) => props.wrap || 'nowrap'}; ;
`;
