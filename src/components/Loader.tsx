import {StyleSheet, Dimensions, View, Modal, Text} from 'react-native';
import React from 'react';
import * as Progress from 'react-native-progress';

interface Props {
  visible: any;
  text: String;
}
const {width} = Dimensions.get('screen');

const Loader: React.FC<Props> = ({visible = false, text}) => {
  return (
    <Modal visible={visible} transparent style={{width: '100%', flex: 1}}>
      <View style={styles.loader}>
        <Progress.CircleSnail
          color={['gold', '#d76f39', 'green']}
          duration={500}
        />
        <Text>{text}</Text>
      </View>
    </Modal>
  );
};

export default Loader;

const styles = StyleSheet.create({
  loader: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 120,
    height: 120,
    borderRadius: 40,
    backgroundColor: 'white',
    position: 'absolute',
    top: 200,
    left: width / 2 - 60,
  },
});
