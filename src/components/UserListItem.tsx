import React from 'react';
import {StyleSheet, Dimensions} from 'react-native';
import {useNavigation} from '@react-navigation/native';
//@ts-ignore
import styled from 'styled-components';
import {StyledText} from './StyledComponents';
import {User} from '../utils/Types';

const {width} = Dimensions.get('screen');
type UserItem = {
  data: User;
  active: boolean;
};
const Container = styled.Pressable`
  width: ${width + 'px'};
  height: 70px;
  justify-content: center;
  padding-horizontal: 20px;
  padding-vertical: 10px;
  border-bottom-width: 0.5px;
  border-color: #e3e3e3;
  flex-direction: row;
`;
const InnerContainer = styled.View`
  margin-left: 20px;
  flex: 1;
  height: 100%;
  justify-content: center;
`;
const NameContainer = styled.View`
  background-color: #d76f39;
  border-radius: 80px;
  width: 45px;
  height: 44px;
  justify-content: center;
  align-items: center;
`;
const Active = styled.View`
  width: 8px;
  height: 8px;
  border-radius: 6px;
  background-color: ${(props: any) => (props.active ? '#3cc260' : '#e0e0e0')};
  position: absolute;
  top: 44px;
  left: 53px;
  border-color: white;
  border-width: 1px;
`;

const UserListItem = ({data, active}: UserItem) => {
  const name = data.name.split(' ');
  const navigation = useNavigation();
  return (
    // @ts-ignore
    <Container onPress={() => navigation.navigate('chat', {data: data})}>
      <NameContainer>
        <StyledText fontSize="15px" color="white">
          {name[0][0] + name[1][0]}
        </StyledText>
      </NameContainer>
      <InnerContainer>
        <StyledText fontSize="14px" color="#d76f39">
          {data.name}
        </StyledText>
        <StyledText color="#d76f39" fontSize="10px">
          {data.contact}
        </StyledText>
      </InnerContainer>
      <Active active={active} />
    </Container>
  );
};

export default UserListItem;

const styles = StyleSheet.create({});
