import React from 'react';
//@ts-ignore
import styled from 'styled-components';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import {StyledText} from './StyledComponents';
import {Text} from 'react-native';

const Mark = styled.View`
  height: 43px;
  width: 43px;
  background-color: ${(props: {you: boolean}) =>
    props.you ? 'rgba(36, 175, 12, 0.8)' : 'rgba(224, 143, 100, 0.8)'};
  border-radius: 25px;
  justify-content: center;
  align-items: center;
  border-color: white;
  border-width: 2px;
`;
const Km = styled.Text`
  color: white;
  font-size: 5px;
  font-weight: bold;
`;
const Name = styled.Text`
  color: white;
  font-size: 6px;
  font-weight: 600;
`;

interface Porps {
  data: any;
  you: boolean;
  distance: number;
}
const CustomMarker = ({data, you, distance}: Porps) => {
  return (
    <Mark you={you}>
      {!you && <Km>{Math.round(distance / 1000) + 'Km'}</Km>}
      <Name>{data.split(' ')[0]}</Name>
    </Mark>
  );
};

export default CustomMarker;
