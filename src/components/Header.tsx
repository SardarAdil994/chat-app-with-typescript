import React from 'react';
import styled from 'styled-components';
import {StyledText} from './StyledComponents';
import {Platform, StatusBar} from 'react-native';

const Container = styled.View`
  width: 100%;
  height: 90px;
  flex-direction: row;
  justify-content: center;
  background-color: #d76f39;
  align-items: center;
  elevation: 8;
  padding-top: ${Platform.OS === 'android'
    ? StatusBar.currentHeight - 5 + 'px'
    : '40px'};
`;

const Header = ({title = '', color}) => {
  return (
    <Container>
      <StatusBar
        barStyle="light-content"
        translucent
        backgroundColor="transparent"
      />
      <StyledText color="white" fontSize="17px" fontWeight="500">
        {title}
      </StyledText>
    </Container>
  );
};

export default Header;
