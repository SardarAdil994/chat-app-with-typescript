import {useState} from 'react';
import {
  Alert,
  KeyboardAvoidingView,
  Platform,
  TouchableOpacity,
} from 'react-native';
import styled from 'styled-components';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {useDispatch} from 'react-redux';
import auth from '@react-native-firebase/auth';

import {
  StyledButton,
  TextInputContainer,
  TextInput,
  StyledText,
  CustomContainer,
} from '../components/StyledComponents';
import {login} from '../utils/redux/Authentication';
import Loader from '../components/Loader';

const BottomContainer = styled.View`
  height: 50%;
  background-color: white;
  position: absolute;
  bottom: 0px;
  width: 100%;
  padding: 20px;
  align-items: center;
  border-top-right-radius: 40px;
  border-top-left-radius: 40px;
`;
const TopContainer = styled.View`
  width: 100%;
  padding: 20px;
  align-items: center;
  flex: 1;
  padding-top: 40px;
`;
const Container = styled.ImageBackground`
  width: 100%;
  flex: 1;
`;

interface Props {
  navigation: any;
}

const Login: React.FC<Props> = ({navigation}) => {
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  const [visible, setVisible] = useState(true);

  const loginFirebase = async () => {
    if (email && password) {
      setIsLoading(true);
      auth()
        .signInWithEmailAndPassword(email, password)
        .then((result: any) => {
          dispatch(login({token: result.user.uid}));
          setIsLoading(false);
        })
        .catch((error: any) => {
          setIsLoading(false);
          Alert.alert('Invalid login detail. Please try again');
          console.error(error);
        });
    } else {
      Alert.alert('Please enter email and password');
      setIsLoading(false);
    }
  };

  return (
    <Container
      source={{
        uri: 'https://images.unsplash.com/photo-1522098635833-216c03d81fbe?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8ODd8fGZyaWVuZHN8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60',
      }}>
      <Loader visible={isLoading} text="login" />
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={{flex: 1}}>
        <TopContainer>
          <MaterialCommunityIcons name="butterfly" size={130} color="white" />
          <StyledText color="white">Friends</StyledText>
        </TopContainer>
        <BottomContainer>
          <StyledText fontSize="20px" fontWeight="bold" color="black">
            Sign in
          </StyledText>
          <TextInputContainer>
            <Ionicons name="person" size={20} color="#d76f39" />
            <TextInput
              placeholder="Email"
              value={email}
              onChangeText={(text: string) => {
                setEmail(text);
              }}
            />
          </TextInputContainer>
          <TextInputContainer>
            <Ionicons name="lock-closed" size={20} color="#d76f39" />
            <TextInput
              placeholder="Password"
              secureTextEntry={visible}
              value={password}
              onChangeText={(text: string) => {
                setPassword(text);
              }}
            />
            <TouchableOpacity onPress={() => setVisible(!visible)}>
              <Ionicons
                name={visible ? 'eye-outline' : 'eye-off-outline'}
                size={18}
                color="#d76f39"
              />
            </TouchableOpacity>
          </TextInputContainer>
          <StyledButton height="45px" onPress={loginFirebase}>
            <StyledText fontSize="15px" fontWeight="bold" color="white">
              Login
            </StyledText>
          </StyledButton>
          <CustomContainer
            style={{flexDirection: 'row'}}
            direction="row"
            justify="space-around">
            <TouchableOpacity>
              <StyledText fontSize="13px" color="#d76f39" line="underline">
                Forgot Password?
              </StyledText>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('signup')}>
              <StyledText fontSize="13px" color="#d76f39" line="underline">
                SignUp
              </StyledText>
            </TouchableOpacity>
          </CustomContainer>
        </BottomContainer>
      </KeyboardAvoidingView>
    </Container>
  );
};
export default Login;
