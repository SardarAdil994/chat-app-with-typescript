import React, {useLayoutEffect, useRef} from 'react';
import {PixelRatio, StatusBar} from 'react-native';
import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import {useSelector, useDispatch} from 'react-redux';
import firestore from '@react-native-firebase/firestore';
import MapViewDirections from 'react-native-maps-directions';
import {getDistance} from 'geolib';

import CustomMarker from '../components/Marker';
import {setAllUsers} from '../utils/redux/Users';

interface Props {
  navigation: any;
}

const Map = ({navigation}: Props) => {
  const dispatch = useDispatch();
  const users = useSelector((state: any) => state.users.allUsers);
  const currentUser = useSelector((state: any) => state.users.currentUser);
  const mapref = useRef(null);
  useLayoutEffect(() => {
    let updatedUsers = [];
    const collectionRef = firestore().collection('users');
    collectionRef.onSnapshot(querySnap => {
      updatedUsers = querySnap.docs.map(doc => {
        return {...doc.data()};
      });
      dispatch(setAllUsers({allUsers: updatedUsers}));
    });
  }, [currentUser]);
  useLayoutEffect(() => {
    //@ts-ignore
    mapref.current.fitToElements({
      edgePadding: {
        top: PixelRatio.getPixelSizeForLayoutSize(30),
        right: PixelRatio.getPixelSizeForLayoutSize(30),
        bottom: PixelRatio.getPixelSizeForLayoutSize(30),
        left: PixelRatio.getPixelSizeForLayoutSize(30),
      },
      animated: false,
    });
  }, [users]);

  return (
    <>
      <StatusBar
        translucent
        barStyle="dark-content"
        backgroundColor="transparent"
      />

      <MapView
        style={{width: '100%', flex: 1, padding: 50}}
        ref={mapref}
        provider={PROVIDER_GOOGLE}
        region={{
          latitude: 31.5204,
          longitude: 74.3587,
          latitudeDelta: 0.05,
          longitudeDelta: 0.05,
        }}>
        {users.map((user: any) => {
          const coords = {
            latitude: user.location.latitude,
            longitude: user.location.longitude,
          };
          if (user.showLocation && user.uid !== currentUser.uid) {
            const distance = getDistance(
              {
                latitude: currentUser.location.latitude,
                longitude: currentUser.location.longitude,
              },
              {...coords},
            );
            return (
              <>
                <Marker
                  onPress={() => {
                    navigation.navigate('chat', {data: user});
                  }}
                  key={user.uid}
                  coordinate={coords}
                  title={user.name}
                  description={user.email}>
                  <CustomMarker
                    data={user.name}
                    you={false}
                    distance={distance}
                  />
                </Marker>
                <MapViewDirections
                  origin={{
                    latitude: currentUser.location.latitude,
                    longitude: currentUser.location.longitude,
                  }}
                  strokeWidth={3}
                  key={user.id}
                  mode="DRIVING"
                  strokeColor="rgba(241, 66, 66, 0.5)"
                  optimizeWaypoints={true}
                  destination={coords}
                  onReady={result => {
                    console.log(
                      result.distance,
                      result.duration,
                      result.waypointOrder,
                    );
                  }}
                  apikey={'AIzaSyC-m5VeGcKV8tUkmOiKN5GMomN9BL9wyw0'}
                />
                {/* <Polyline
                  coordinates={[
                    {
                      latitude: currentUser.location.latitude,
                      longitude: currentUser.location.longitude,
                    },
                    coords,
                  ]}
                  key={user.id + currentUser.id}
                  strokeColor="rgba(0, 0, 0, 0.8)"
                  strokeColors={['rgba(224, 143, 100, 0.8)']}
                  strokeWidth={2}
                /> */}
              </>
            );
          } else if (user.showLocation && user.uid === currentUser.uid) {
            return (
              <Marker
                onPress={() => {}}
                key={user.uid}
                coordinate={coords}
                title={user.name}
                description={user.email}>
                <CustomMarker data={'Me'} you={true} distance={-1} />
              </Marker>
            );
          }
        })}
      </MapView>
    </>
  );
};

export default Map;
