import {useState} from 'react';
import styled from 'styled-components';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  Alert,
  Modal,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import {useDispatch} from 'react-redux';
import MapView, {Marker} from 'react-native-maps';
import GetLocation from 'react-native-get-location';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import messaging from '@react-native-firebase/messaging';

import {
  StyledButton,
  TextInputContainer,
  TextInput,
  StyledText,
  CustomContainer,
} from '../components/StyledComponents';
import {login} from '../utils/redux/Authentication';
import Loader from '../components/Loader';

const BottomContainer = styled.View`
  height: 525px;
  background-color: white;
  margin-top: 190px;
  width: 100%;
  padding: 20px;
  align-items: center;
  border-top-right-radius: 40px;
  border-top-left-radius: 40px;
`;
const Container = styled.ImageBackground`
  width: 100%;
  flex: 1;
`;
const InnerContainer = styled.ScrollView`
  flex: 1;
`;

const Button = styled.TouchableOpacity`
  position: absolute;
  bottom: 10px;
  right: 10px;
  background-color: #d76f39;
  justify-content: center;
  align-items: center;
  border-radius: 40px;
  padding: 10px;
`;
const LocationInputContainer = styled.Pressable`
  width: 85%;
  height: 30px;
  margin-vertical: 20px;
  border-bottom-width: 1px;
  border-bottom-color: grey;
  padding: 0px;
  flex-direction: row;
  align-items: center;
  background-color: ${(props: any) => props.color || 'transparent'};
`;
const ModalContainer = styled.View`
  background-color: rgba(0, 0, 0, 0.7);
  width: 100%;
  height: 100%;
`;

const Cross = styled.TouchableOpacity`
  position: absolute;
  top: 10px;
  left: 10px;
`;
interface Props {
  navigation: any;
  prevState: null;
}
export default function Login({navigation}: Props) {
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  const [name, setName] = useState('');
  const [contact, setConatct] = useState('');
  const [modal, setModal] = useState(false);
  const [location, setLocation] = useState<any | null>(null);
  const [visible, setVisible] = useState(true);

  const locationTrack = async () => {
    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000,
    })
      .then(location => {
        setLocation(location);
      })
      .catch(error => {
        const {code, message} = error;
        console.warn(code, message);
      });
  };
  const getToken = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      const token = await messaging().getToken();
      return token;
    }
  };
  const signUp = async () => {
    setIsLoading(true);
    const token = await getToken();
    if (email && name && password && location && contact) {
      const result = await auth().createUserWithEmailAndPassword(
        email,
        password,
      );
      firestore()
        .collection('users')
        .add({
          name: name,
          email: email,
          isActive: true,
          password: password,
          contact: contact,
          location: {
            latitude: location.latitude,
            longitude: location.longitude,
          },
          uid: result.user.uid,
          showLocation: true,
          deviceToken: token,
        });
      dispatch(login({token: result.user.uid}));
      setIsLoading(false);
    } else {
      setIsLoading(false);
      Alert.alert('Please fill all the details');
    }
  };
  return (
    <Container
      source={{
        uri: 'https://images.unsplash.com/photo-1522098635833-216c03d81fbe?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8ODd8fGZyaWVuZHN8ZW58MHx8MHx8&auto=format&fit=crop&w=500&q=60',
      }}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={{flex: 1}}>
        <InnerContainer showsVerticalScrollIndicator={false}>
          <Modal visible={modal} transparent>
            <ModalContainer>
              <MapView
                style={{width: '100%', height: '100%'}}
                region={
                  location
                    ? {
                        latitude: location?.latitude,
                        longitude: location?.longitude,
                        latitudeDelta: 0.0022,
                        longitudeDelta: 0.0421,
                      }
                    : {
                        latitude: 31.582045,
                        longitude: 74.329376,
                        latitudeDelta: 0.122,
                        longitudeDelta: 0.1421,
                      }
                }>
                {location && (
                  <Marker
                    coordinate={{
                      latitude: location?.latitude,
                      longitude: location?.longitude,
                    }}
                  />
                )}
              </MapView>
              <Button onPress={locationTrack}>
                <Text>Use current loaction</Text>
              </Button>
              <Cross onPress={() => setModal(false)}>
                <Ionicons name="md-arrow-undo" size={40} color="#d76f39" />
              </Cross>
            </ModalContainer>
          </Modal>
          <Loader visible={isLoading} text="" />

          <BottomContainer>
            <StyledText fontSize="20px" fontWeight="bold" color="black">
              Sign Up
            </StyledText>
            <TextInputContainer>
              <Ionicons name="person" size={20} color="#d76f39" />
              <TextInput
                placeholder="Name"
                value={name}
                onChangeText={(text: string) => {
                  setName(text);
                }}
              />
            </TextInputContainer>
            <TextInputContainer>
              <Ionicons name="mail" size={20} color="#d76f39" />
              <TextInput
                placeholder="Email"
                value={email}
                onChangeText={(text: string) => {
                  setEmail(text);
                }}
              />
            </TextInputContainer>
            <TextInputContainer>
              <Ionicons name="ios-call" size={20} color="#d76f39" />
              <TextInput
                placeholder="Contact"
                value={contact}
                onChangeText={(text: string) => {
                  setConatct(text);
                }}
              />
            </TextInputContainer>
            <LocationInputContainer onPress={() => setModal(true)}>
              <Ionicons name="location" size={20} color="#d76f39" />
              <TextInput
                editable={false}
                placeholder={location ? 'Press to see on map' : 'Location'}
                value={location}
                onChangeText={(text: string) => {
                  setEmail(text);
                }}
              />
            </LocationInputContainer>
            <TextInputContainer>
              <Ionicons name="lock-closed" size={20} color="#d76f39" />
              <TextInput
                placeholder="Password"
                secureTextEntry={visible}
                value={password}
                onChangeText={(text: string) => {
                  setPassword(text);
                }}
              />
              <TouchableOpacity onPress={() => setVisible(!visible)}>
                <Ionicons
                  name={visible ? 'eye-outline' : 'eye-off-outline'}
                  size={18}
                  color="#d76f39"
                />
              </TouchableOpacity>
            </TextInputContainer>
            <StyledButton height="45px" onPress={signUp}>
              <StyledText fontSize="15px" fontWeight="bold" color="white">
                SignUp
              </StyledText>
            </StyledButton>
            <CustomContainer
              style={{flexDirection: 'row'}}
              direction="row"
              justify="space-around">
              <TouchableOpacity>
                <StyledText fontSize="13px" color="#d76f39" line="underline">
                  Forgot Password?
                </StyledText>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                <StyledText fontSize="13px" color="#d76f39" line="underline">
                  SignIn
                </StyledText>
              </TouchableOpacity>
            </CustomContainer>
          </BottomContainer>
        </InnerContainer>
      </KeyboardAvoidingView>
    </Container>
  );
}
