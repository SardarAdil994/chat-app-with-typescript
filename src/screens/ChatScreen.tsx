import React, {useLayoutEffect, useState, useRef} from 'react';
import {GiftedChat, Bubble} from 'react-native-gifted-chat';
import {useSelector} from 'react-redux';
import * as Progress from 'react-native-progress';
import {View, Alert} from 'react-native';
import firestore from '@react-native-firebase/firestore';

import {RootState} from '../utils/redux/Store';
import axios from 'axios';
interface Props {
  navigation: any;
  route: any;
}

const Chat = ({route}: Props) => {
  const you = useSelector((state: RootState) => state.users.currentUser);
  const user = route.params.data;
  const [lastMessage, setLastMessage] = useState<any>(null);
  const [messages, setMessages] = useState<any>([]);
  const chatRef = useRef(null);
  const [text, setText] = useState('');
  const [isEditing, setIsEditing] = useState(false);

  useLayoutEffect(() => {
    let newMessages = [];
    const docId =
      you.uid > user.uid ? user.uid + '-' + you.uid : you.uid + '-' + user.uid;
    const messagesRef = firestore()
      .collection('chatRooms')
      .doc(docId)
      .collection('messages')
      .orderBy('createdAt', 'desc');
    messagesRef.onSnapshot(querySnap => {
      newMessages = querySnap.docs.map(doc => {
        if (doc.data().user._id === you.uid) {
          if (!lastMessage) {
            setLastMessage({
              ...doc.data(),
              createdAt: doc.data().createdAt.toDate(),
              id: doc.id,
            });
          }
        }
        return {
          ...doc.data(),
          createdAt: doc.data().createdAt.toDate(),
          id: doc.id,
        };
      });
      messages.length > 0
        ? //@ts-ignore
          GiftedChat.append(newMessages[0])
        : setMessages(newMessages);
    });
  }, [user, you]);
  const sendNotification = async (message: any) => {
    try {
      const resp = await axios.post(
        'https://fcm.googleapis.com/fcm/send',
        {
          to: user.deviceToken,
          notification: {
            title: you.name,
            body: message.text,
            mutable_content: true,
            sound: 'Tri-tone',
          },
          data: {
            url: 'chat',
            id: you.uid,
            deviceToken: you.deviceToken,
          },
        },
        {
          headers: {
            'content-type': 'application/json',
            Authorization:
              'key=AAAAny8G6qU:APA91bFpb2_nZ5_OX0OZOS-HJ4GshKZHuzbNHOiv16KJuquQK9gDcaMcfgPJavCVOJfsG9b6_sJYzo-zG8zJUKDI3uzJTsb-oxDm9dOa0IvOaKdJOtvFqb2vu9DLdjAQiXBY5F0iNsic',
          },
        },
      );
    } catch (err) {
      console.log(err, user.deviceToken);
    }
  };
  const onSend = async (messages = []) => {
    if (!isEditing) {
      sendNotification(messages[0]);
      const message: any = messages[0];
      const myMessage = {
        ...message,
        sentBy: you.uid,
        sentTo: user.uid,
      };
      //@ts-ignore
      GiftedChat.append(myMessage);
      const docId =
        you.uid > user.uid
          ? user.uid + '-' + you.uid
          : you.uid + '-' + user.uid;
      firestore()
        .collection('chatRooms')
        .doc(docId)
        .collection('messages')
        .add({...myMessage, createdAt: firestore.Timestamp.now()});
    } else {
      const docId =
        you.uid > user.uid
          ? user.uid + '-' + you.uid
          : you.uid + '-' + user.uid;
      firestore()
        .collection('chatRooms')
        .doc(docId)
        .collection('messages')
        .doc(lastMessage.id)
        .update({
          ...lastMessage,
          text: text,
        });
      setIsEditing(false);
    }
  };
  const onLongPress = (context: any, message: any) => {
    const docId =
      you.uid > user.uid ? user.uid + '-' + you.uid : you.uid + '-' + user.uid;
    if (lastMessage.id === message.id) {
      Alert.alert('Message', 'Are you want to delete or update message', [
        {
          text: 'Delete',
          onPress: async () => {
            const response = await firestore()
              .collection('chatRooms')
              .doc(docId)
              .collection('messages')
              .doc(message.id)
              .delete();
            setLastMessage(null);
          },
        },
        {
          text: 'Update',
          onPress: () => {
            if (lastMessage.id === message.id) {
              setIsEditing(true);
              setText(message.text);
            }
          },
        },
        {
          text: 'cancel',
        },
      ]);
    } else {
      Alert.alert('Message', 'Are you want to delete or update message', [
        {
          text: 'Delete',
          onPress: async () => {
             await firestore()
              .collection('chatRooms')
              .doc(docId)
              .collection('messages')
              .doc(message.id)
              .delete();
          },
        },
        {
          text: 'cancel',
        },
      ]);
    }
  };
  return (
    <>
      <GiftedChat
        ref={chatRef}
        text={text}
        onInputTextChanged={txt => {
          setText(txt);
        }}
        onLongPress={onLongPress}
        renderLoading={() => (
          <View
            style={{
              width: '100%',
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Progress.CircleSnail color={['red', '#d76f39', 'orange']} />
          </View>
        )}
        messages={messages}
        showAvatarForEveryMessage={true}
        messagesContainerStyle={{backgroundColor: 'white'}}
        renderBubble={(props: any) => (
          <Bubble
            {...props}
            wrapperStyle={{
              right: {
                backgroundColor: '#d76f39',
                borderColor: 'white',
              },
            }}
          />
        )}
        onSend={(messages: any) => onSend(messages)}
        user={{
          _id: you.uid,
          name: you.name,
        }}
      />
    </>
  );
};

export default Chat;
