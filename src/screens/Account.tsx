import React, {useState} from 'react';
import {Switch} from 'react-native';
import {useSelector} from 'react-redux';
import styled from 'styled-components';
import {useDispatch} from 'react-redux';
import firestore from '@react-native-firebase/firestore';

import {StyledText} from '../components/StyledComponents';
import {logout} from '../utils/redux/Authentication';
import Header from '../components/Header';
import {RootState} from '../utils/redux/Store';

const Container = styled.View`
  width: 100%;
  flex: 1;
  background-color: white;
  align-items: center;
`;

const NameContainer = styled.View`
  background-color: #d76f39;
  border-radius: 80px;
  width: 105px;
  height: 105px;
  justify-content: center;
  align-items: center;
  margin-top: 20px;
`;

const Button = styled.TouchableOpacity`
  width: 60%;
  height: 44px;
  background-color: #d76f39;
  border-radius: 40px;
  justify-content: center;
  align-items: center;
  margin-top: 60px;
`;

const LocationContainer = styled.View`
  flex-direction: row;
  align-items: center;
`;

const Account = () => {
  const dispatch = useDispatch();
  const user = useSelector((state: RootState) => state.users.currentUser);
  const [isEnabled, setIsEnabled] = useState(user?.showLocation);
  const toggleSwitch = async () => {
    setIsEnabled((previousState: any) => !previousState);
    const response = await firestore()
      .collection('users')
      .doc(user?.id)
      .update({
        ...user,
        isActive: true,
        showLocation: !isEnabled,
      });
  };
  const logoutFucntion = async () => {
    await firestore()
      .collection('users')
      .doc(user?.id)
      .update({
        ...user,
        isActive: false,
      });
    dispatch(logout());
  };

  return (
    <>
      <Container>
        <Header title="Account" color="#d76f39" />
        <NameContainer>
          <StyledText color="white" fontSize="25px">
            {user?.name[0]}
          </StyledText>
        </NameContainer>
        <StyledText color="#d76f39" fontSize="25px">
          {user?.name}
        </StyledText>
        <StyledText color="#d76f39" fontSize="14px">
          {user?.email}
        </StyledText>
        <StyledText color="#d76f39" fontSize="15px">
          {user?.contact}
        </StyledText>
        <LocationContainer>
          <StyledText color="#d76f39" fontSize="15px">
            Location
          </StyledText>
          <Switch
            trackColor={{false: '#767577', true: '#d76f39'}}
            thumbColor={'white'}
            ios_backgroundColor="white"
            onValueChange={toggleSwitch}
            value={isEnabled}
            style={{width: 50}}
          />
        </LocationContainer>
        <Button onPress={() => logoutFucntion()}>
          <StyledText color="white" fontSize="15px">
            Logout
          </StyledText>
        </Button>
      </Container>
    </>
  );
};

export default Account;
