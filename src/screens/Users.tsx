import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet, AppState, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import firestore from '@react-native-firebase/firestore';

import UserListItem from '../components/UserListItem';
import Header from '../components/Header';
import Loader from '../components/Loader';
import {setAllUsers} from '../utils/redux/Users';
import {RootState} from '../utils/redux/Store';
import {Listener} from '../utils/Notifivations';
import {useNavigation} from '@react-navigation/native';
import {LocalNotificationListener} from '../utils/LocalNotificationListener';

const Users = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [aState, setAState] = useState(AppState.currentState);
  const usersData = useSelector((state: RootState) => state.users.allUsers);
  const currentUser = useSelector(
    (state: RootState) => state.users.currentUser,
  );
  useEffect(() => {
    let updatedUsers = [];
    const collectionRef = firestore().collection('users');
    collectionRef.onSnapshot(querySnap => {
      updatedUsers = querySnap.docs.map(doc => {
        return {...doc.data(), id: doc.id};
      });
      dispatch(setAllUsers({allUsers: updatedUsers}));
    });
    const subscription = AppState.addEventListener(
      'change',
      handleAppStateChange,
    );
    return () => {
      subscription.remove();
    };
  }, [currentUser]);

  useEffect(() => {
    Listener(navigation);
    LocalNotificationListener(navigation);
  }, []);

  const handleAppStateChange = async (nextAppState: any) => {
    try {
      firestore()
        .collection('users')
        .doc(currentUser?.id)
        .update({
          ...currentUser,
          isActive: nextAppState === 'active' ? true : false,
        });
      setAState(nextAppState);
    } catch (error) {
      console.log(error, currentUser?.id);
    }
  };
  return (
    <View style={styles.container}>
      <Header title="Chats" color="#d76f39" />
      {usersData ? (
        <FlatList
          data={usersData}
          keyExtractor={item => item?.id}
          //@ts-ignore
          renderItem={(item: any) => {
            if (currentUser?.id !== item.item.id) {
              return (
                <UserListItem data={item?.item} active={item?.item?.isActive} />
              );
            }
          }}
        />
      ) : (
        <>
          <Loader visible={false} text="Loading" />
        </>
      )}
    </View>
  );
};

export default Users;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    flex: 1,
    backgroundColor: 'white',
  },
});
